# pozaziemscy-zaborcy-rs
This project is an attempt to rewrite [Pozaziemscy Zaborcy](https://github.com/ZSAInfProject/PozaziemscyZaborcy) project to Rust.

## Why?
It turns out that pygame is suboptimal for modern game development as it introduces too much overhead. We used a code profiler and considered various parts of pygame to be major bottlenecks. As such, we have decided to have a look at Rust.

## Getting started
1. [Install rust](https://www.rust-lang.org/en-US/index.html)
2. Clone the repo
3. `$ cargo run`

### Notes
Instalation of both [`rustfmt`](https://github.com/rust-lang-nursery/rustfmt) and [`clippy`](https://github.com/rust-lang-nursery/rust-clippy) is advised.
```
$ rustup add component clippy-preview
$ rustup add component rustfmt-preview
```

`rustfmt` is a tool that formats your code according to rust code formating guidelines.

`clippy` is a tool that analyses the code and prints you tips on how to make your code more idiomatic (overall better).

**Usage**
```
$ cargo clippy
$ cargo fmt
```

Right now, there are 2 places in which we can make our code better. Run `clippy` to see them!